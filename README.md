# ics-ans-role-conda

Ansible role to install conda on CentOS and Debian.

This role includes as well:

- a `conda` module that can be used to manage conda packages.
- a `conda_env` module that can be used to manage conda environments.

## Role Variables

```yaml
conda_version: 4.8.4
conda_owner: root
conda_group: root
conda_channel_alias: https://artifactory.esss.lu.se/artifactory/api/conda
# List of conda channels to use
conda_channels:
  - conda-e3
  - ics-conda-forge
  - conda-forge
  - anaconda-main
# List of conda environment yaml files to create
conda_env_files: []
# Name of the conda environment to activate by default
# If left empty no conda env is activated
conda_default_env: ""
```

The role can create conda environments if you pass a list of yaml environment files via
the `conda_env_files` variable.
You can use both local files and http/https url:

```yaml
conda_env_files:
  - "{{ playbook_dir }}/config/molecule_env.yml"
  - https://gitlab.esss.lu.se/ics-infrastructure/conda-environments/raw/master/molecule_env.yml
```

Note that this is deprecated as it has some drawbacks:

- the update is only based on the file content (via a handler) and not the environment itself
- handler can't be used when using `include_role`

Using the `conda_env` module is the recommended way.

## conda module

The `conda` module allows to install, update or remove conda packages.

```yaml
- name: install flask 1.0 and Python 3.7
  conda:
    name:
      - python=3.7
      - flask=1.0
    state: present
    environment: myapp

- name: install flask from conda-forge
  conda:
    name: flask
    state: present
    environment: flaskapp
    channels:
      - conda-forge

- name: update flask to the latest version
  conda:
    name: flask
    state: latest
    environment: myapp

- name: update conda to the latest version
  conda:
    name: conda
    state: latest

- name: remove flask from myapp environment
  conda:
    name: flask
    state: absent
    environment: myapp
```

## conda_env module

The `conda_env` module allows to create, update or remove conda environments.

```yaml
- name: create myenv environment
  conda_env:
    name: myenv
    state: present
    file: /tmp/environment.yml

- name: update all packages in myenv environment
  conda_env:
    name: myenv
    state: latest

- name: update myenv environment based on environment.yml
  conda_env:
    name: myenv
    state: present
    prune: true
    file: /tmp/environment.yml

- name: force the environment creation (remove previous one first)
  conda_env:
    state: present
    force: true
    file: /tmp/environment.yml

- name: remove myenv environment
  conda_env:
    name: myenv
    state: absent
```

Note that the environment.yml file shall be present locally on the host.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conda
```

## License

BSD 2-clause
